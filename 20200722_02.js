// Run by Node.js

const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.on("line", function(line) {

	var first = 0;
	var beforeStr = line;
	var afterStr = beforeStr.split(' ');
	var a = Number(afterStr[0]);
	var b = Number(afterStr[1]);
	var n = Number(afterStr[2]);
	for( var i=a; i<=b; i++ ){
		first = first + Math.pow(n, i);
		
	}
	
console.log( first );

	rl.close();
}).on("close", function() {
	process.exit();
});