function solution(answers) {
    var answer = [];
    var arr1 = [1,2,3,4,5];
    var arr2 = [2,1,2,3,2,4,2,5];
    var arr3 = [3,3,1,1,2,2,4,4,5,5];
    var arrResult1 = 0;
    var arrResult2 = 0;
    var arrResult3 = 0;
    
    for( var i=0; i< answers.length; i++ ){
        if(answers[i] == arr1[i]){
            arrResult1 =  arrResult1 + 1;
        }
        if(answers[i] == arr2[i]){
            arrResult2 =  arrResult2 + 1;
        }
        if(answers[i] == arr3[i]){
            arrResult3 =  arrResult3 + 1;
        }
    }
    var arr = [arrResult1  , arrResult2 , arrResult3]
    var max = Math.max.apply(null,arr);
     
    if(max == arrResult1){
        answer.push(1)
    }
    if(max == arrResult2){
        answer.push(2)
    }
    if(max == arrResult3){
        answer.push(3)
    }
    console.log(  arrResult1  , arrResult2 , arrResult3)
    console.log(max )
    return answer;
}