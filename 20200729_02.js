// Run by Node.js

const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.on("line", function(line) {
	var aa = line;
	var bb = aa.split('').reverse().join('');
	
	if( aa == bb ){
		console.log('Palindrome');
	}else {
		console.log('Not Palindrome');
	}
	rl.close();
}).on("close", function() {
	process.exit();
});