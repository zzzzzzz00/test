function solution(s) {
    var a = s.length / 2 ,
        b = Math.ceil(s.length / 2);
    if(s.length % 2 === 0){
        return s.substring( a - 1 , a + 1)
    }else{
        return s.substring( b - 1 , b)
    }
}