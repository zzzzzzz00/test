// Run by Node.js

const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.on("line", function(line) {
	var beforeStr = line;
var afterStr = beforeStr.split(' ');
	var a = Number(afterStr[0]);
	var b = Number(afterStr[2]);
	var result;
	switch(afterStr[1]){
		case "+":
			result = a + b;
			break;
		case "-":
			result = a - b;
			break;
		case "*":
			result = a * b;
			break;
		case "/":
			result = a / b;
			break;
	}
	
	console.log(result);
	rl.close();
}).on("close", function() {
	process.exit();
});